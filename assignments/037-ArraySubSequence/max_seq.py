def max_seq(l):
    """
    max_seq returns a the maximum increasing contiguous subsequence in the list.
    (as determined by length).  If two or more subsequences have equal lengths,
    return the first subsequence found.
   
    Args:
    l(list): list of numbers

    Returns:
    the maximum increasing contiguous subsequence as a list. If the l is empty,
    an empty list is returned

    Raises:
    TypeError if the list contains an item that is not arithmetically 
    compatible with ints and floats
    """
