{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "96dd2ee0",
   "metadata": {},
   "source": [
    "<div class=\"pagebreak\"></div>\n",
    "\n",
    "# Recursion\n",
    "Recursion is an algorithmic technique in which the solution defines itself in terms of the solution itself. A recursive solution continually solves smaller and smaller instances of itself until the solution reaches a base case.\n",
    "\n",
    "Suppose you are waiting in line - \n",
    "![](images/recursion/line.png)\n",
    "\n",
    "a very long line - \n",
    "![](images/recursion/long_line.png)\n",
    "\n",
    "How can you figure out how many people are in front of you?\n",
    "- You are not allowed to move\n",
    "- You can only see the the person in front of you and behind you\n",
    "- You can communicate with the person in front and behind you\n",
    "\n",
    "Apply recursion!\n",
    "\n",
    "number of people in front of you = # of people in front of the person in front of you + person in front of you\n",
    "\n",
    "Solution:\n",
    "- Tap the shoulder of the person in front of you and ask how many people are in front of him/her\n",
    "- Wait for his/her response and then add 1\n",
    "- If someone behinds you asks, tell them how many people are in front of you\n",
    "\n",
    "Recursive algorithms have two main components:\n",
    "- **Base Case**: Point where you stop applying the recursive and can answer the question directly\n",
    "- **Recursive Case**: Set of instructions that will be executed repeatedly with smaller versions of the problem\n",
    "\n",
    "\n",
    "Line count:\n",
    "- **Base case**: no one is in front of me.  Return 0\n",
    "- **Recursive case**: return number of people in front of me + 1 (recursive call)\n",
    "\n",
    "\n",
    "From a practical point of view with programming, a recursive function is any function that calls itself. Indirect recursion exists when a function invokes another function before the original function is called again without any returns. Recursive solutions will have cycles (loops) within the call graph.\n",
    "\n",
    "For the line count, suppose we represent each individual as an entry in a list.  The recursive solution would then look like - \n",
    "```python\n",
    "def count_entries(my_list):\n",
    "    if len(my_list) == 0:\n",
    "        return 0\n",
    "    else:\n",
    "        return count_entries(my_list[0:-1]) + 1\n",
    "\n",
    "print(count_entries(list(range(10))))\n",
    "\n",
    "```\n",
    "\n",
    "As you can see from the repetitive calls, recursion brings back the concept of repetition.  Revisiting Python's loop statements (`while` and `for`), both had methods to be able to stop the loop somehow.  `while` has a condition check before we began each iteration of the loop.  `for` has an implicit end to iteration once the sequence completes. Similarly, for recursive functions, we need to define a base case that allows the repetition to stop.  This base case will be the simplest case of the problem itself: \n",
    "- _numerical_: when computing a numeric answer, generally the base case will be for 0 or 1.\n",
    "- _structural_: when processing data structures(lists, dictionaries, sets, etc.), the base case exists when that structure is empty or has no members of a particular type\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef45075f",
   "metadata": {},
   "source": [
    "The line counting used structural recursion as the recursive call processed smaller and smaller versions of the list.  (And, yes, we could have simply just called `len(my_list)` to get the number of entries in the list.)\n",
    "\n",
    "Computing factorials is the prototypical example to demonstrate numeric recursion.  The factorial of a non-negative integer $n$, denoted by $n!$, is the product of all positive integers less than or equal to $n$. The factorial of $n$ also equals the product of $n$ with the next smaller factorial: \n",
    "\n",
    "$n! = n\\times (n-1)\\times (n-2)\\times (n-3)\\times \\cdots \\times 3\\times 2\\times 1 = n\\times (n-1)!$\n",
    "\n",
    "For example: $5!=5\\times 4\\times 3\\times 2\\times 1=5\\times 24=120$\n",
    "\n",
    "The value of $0!$ is $1$.\n",
    "\n",
    "As you can see from the description, we have both the base case and the recursive case present in the definition.\n",
    "- **Base case**: $0! = 1$\n",
    "- **Recursive case**: $n! = n \\times (n-1)$\n",
    "\n",
    "Here's the Python implementation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64f1bc59",
   "metadata": {},
   "outputs": [],
   "source": [
    "def factorial(n):\n",
    "    \"\"\"Return the factorial of n.\"\"\"\n",
    "    if n == 0:\n",
    "        return 1\n",
    "    else:\n",
    "        return n * factorial(n-1)\n",
    "\n",
    "print(factorial(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f4c992a",
   "metadata": {},
   "source": [
    "[View execution on PythonTutor](https://pythontutor.com/render.html#code=def%20factorial%28n%29%3A%0A%20%20%20%20%22%22%22Return%20the%20factorial%20of%20n.%22%22%22%0A%20%20%20%20if%20n%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%201%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20return%20n%20*%20factorial%28n-1%29%0A%0Aprint%28factorial%285%29%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)\n",
    "\n",
    "As you can see from the factorial example, the `def` statement header looks just like other functions.  We then have a conditional statement (`if n == 0:`) to check for the base case - these are evaluated without any further recursive call.  Finally we have the recursive case (`return n * factorial(n-1)`) in which we solve for n in terms of a smaller version of the problem (`factorial(n-1)`)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "431b3e3a",
   "metadata": {},
   "source": [
    "## Recursion Rules\n",
    "Recursive algorithms must follow three rules:\n",
    "- a recursive algorithm must call itself (recursive case)\n",
    "- a recursive algorithm must have a base case\n",
    "- a recursive algorithm must change its state and move towards the base case.\n",
    "\n",
    "Look back over the line counting and factorial examples.  How were these rules applied?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ed16bbe",
   "metadata": {},
   "source": [
    "Before you run the following code block, what do you think occurs? Does `say_hello()` follow the recursion rules? \n",
    "\n",
    "Go ahead and run it now.  Seriously.  Your computer won't crash."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3333606",
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello():\n",
    "    print(\"hello\")\n",
    "    say_hello()\n",
    "    \n",
    "say_hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f11138f0",
   "metadata": {},
   "source": [
    "You should have seen \"hello\" printed a large number of times followed by an error condition at the bottom of the output:\n",
    "```\n",
    "RecursionError: maximum recursion depth exceeded while calling a Python object\n",
    "```\n",
    "As a program makes function calls, Python must add a \"stack frame\" to keep track of the current function and its predecessors. This stack frame allows the interpreter to keep track of local namespaces for each function call. Before creating another “stack frame”, the interpreter checks how many instances exist – the number of function calls made without a corresponding return. If that number is beyond a certain threshold, then the Python interpreter assumes that a logic error has occurred, stops execution, and reports the recursion error. This \"depth\" is configurable. Other languages implement this check differently. For example, Java maintains a separate memory space and will have a memory error when the stack space is exhausted. The C and C++ standards do not specify a limit; the environment (operating system, other libraries, etc.) in which the program runs determines the available stack space.\n",
    "\n",
    "`say_hello()` violated two of the recursive rules: no base case and no change in state to move towards the base case.\n",
    "\n",
    "For printing a message, we track how many times the message has been printed (or, preferably, how many more times the message needs to be printed) and stop when that number is reached. The examples here will show both keeping track of a decreasing count as well as an increasing count.  By convention(and \"solving smaller instances of itself\"), programmers write recursive functions with decreasing values. Of course, we could use a `for` loop with a `range()` to print the message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2d4617c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello_increasing(count, max_count):\n",
    "    if (count == max_count):\n",
    "        return\n",
    "    say_hello_increasing(count + 1, max_count)\n",
    "    print('hello')\n",
    "    \n",
    "say_hello_increasing(0,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02dd0c73",
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello_decreasing(count):\n",
    "    if (count == 0):\n",
    "        return\n",
    "    print(\"hello\")\n",
    "    say_hello_decreasing(count - 1)\n",
    "    \n",
    "say_hello_decreasing(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a73a09e",
   "metadata": {},
   "source": [
    "Notice that the latter function is also less complex in that we did not need to track the maximum number of messages to print.  Our initial call implicitly defines this value.\n",
    "\n",
    "[View say hello decreasing on pythontutor.com](https://pythontutor.com/render.html#code=def%20say_hello_decreasing%28count%29%3A%0A%20%20%20%20if%20%28count%20%3D%3D%200%29%3A%0A%20%20%20%20%20%20%20%20return%0A%20%20%20%20print%28%22hello%22%29%0A%20%20%20%20say_hello_decreasing%28count%20-%201%29%0A%20%20%20%20%0Asay_hello_decreasing%285%29&cumulative=false&curInstr=0&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0df00d7",
   "metadata": {},
   "source": [
    "## Defensive Programming\n",
    "Defensive programming is the practice of writing code such that the code continues to function correctly even if used in a manner that was not originally intended or imagined. Defensive programming strives to reduce the number of potential problems by using slightly different logic that still meets the original intent.\n",
    "\n",
    "As mentioned, Python is a dynamically, strongly typed language. By dynamic, the types are determined at runtime. By strongly typed, the interpreter enforces which operations variables and expressions may participate based on type. In contrast, C, C++, and Java are statically typed. C is considered weakly typed as type checking is not strongly enforced - an assumption exists that programmers know what they are doing. C++ has much stronger type checking during the compilation process and is considered strongly typed. Java has both static checks during compilation as well as runtime checks. [Ada](https://en.wikipedia.org/wiki/Ada_(programming_language) is most likely the strongest-typed language.\n",
    "\n",
    "As one of the effects of being dynamically  typed, it is possible to use different types than  the programmer originally intended as long as the operations or methods called on that type are supported.  \n",
    "\n",
    "What happens if we make this function call - `say_hello_decreasing(5.5)`?  How many messages will appear?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0f161b54",
   "metadata": {},
   "outputs": [],
   "source": [
    "say_hello_decreasing(5.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fca8b078",
   "metadata": {},
   "source": [
    "oops...\n",
    "\n",
    "Since `say_hello_decreasing()` had an equality check against zero, that condition never evaluated to `True` and the base condition was \"blown through\".\n",
    "\n",
    "We can modify the condition to be more \"forgiving\" and, hence, prevent potential mishaps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db012a3a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello_defensive(count):\n",
    "    if (count <= 0):\n",
    "        return\n",
    "    print(\"hello\")\n",
    "    say_hello_defensive(count - 1)\n",
    "    \n",
    "say_hello_defensive(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "443e449d",
   "metadata": {},
   "source": [
    "A slightly different but equivalent version.  Two differences: \n",
    "1. The function performs some activity first.\n",
    "2. Function checks for the base case before making the recursive call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8cac5ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello_defensive_alternate(count):\n",
    "    print(\"hello\")\n",
    "    if (count > 0):\n",
    "        say_hello_defensive_alternate(count - 1)\n",
    "    \n",
    "say_hello_defensive_alternate(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2b903e0",
   "metadata": {},
   "source": [
    "Which approach is better? As with just about any answer with consulting, it depends. The second function will always print at least one message. What is the desired behavior?  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d12bcfe",
   "metadata": {},
   "source": [
    "## Example: Summing a List\n",
    "While recursion is not an optimal way to sum a list (a `for` loop is more appropriate, using the built-in function `sum` is the best approach), programmers can use recursion for this task.\n",
    "\n",
    "So to approach a recursive solution, revisit the Seven Steps and work through some sample cases by hand first.\n",
    "\n",
    "The sum of an empty list is 0. The sum of a list with just one element is that element itself. A negative length for a list is illogical.\n",
    "\n",
    "So how about a list of four numbers: $[a, b, c, d]$?\n",
    "\n",
    "$sum = a + b + c + d $\n",
    "\n",
    "Applying associativity, what are the different possibilities?\n",
    "\n",
    "$sum = a + b + (c + d) $<br>\n",
    "$sum = a + (b + c + d) $<br>\n",
    "$sum = (a + b) + (c + d) $<br>\n",
    "$sum = (a + b + c) + d $<br>\n",
    "$sum = (a + b ) + c + d $\n",
    "\n",
    "The second and fourth possibilities might be a good pattern for us.  Take the second - so the sum equals the first number $a$ plus the rest of the list $[b,c,d]$  We have just created a smaller instance.  Now, the sum of $[b,c,d] = b + c + d = b + (c + d)$\n",
    "\n",
    "Now, write pseudocode for this:\n",
    "<pre>\n",
    "    input: list of numbers\n",
    "    output: sum\n",
    "    algorithm:\n",
    "      function sum_list(list)\n",
    "         if length(list) = 0, return 0\n",
    "         else if length(list) = 1, return the first element (list[0])\n",
    "         else return list[0] + sum of the rest of the list\n",
    "</pre>\n",
    "\n",
    "Anything to generalize? The condition for the list with only one element could be expressed as the first number plus an empty list. That simplifies our logic at the small cost of an extra function call. \n",
    "\n",
    "Step through a couple of sample lists manually to see how our approach works:  []   [5]   [1,2]  [1,2,3]\n",
    "\n",
    "Time to translate the pseudocode to Python.  Remember how to slice lists?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b075487b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def sum_list(list_to_sum):\n",
    "    if len(list_to_sum) == 0:\n",
    "        return 0\n",
    "    return list_to_sum[0] + sum_list(list_to_sum[1:])\n",
    "\n",
    "print(\"[]:\", sum_list([]))\n",
    "print(\"[5]:\", sum_list([5]))\n",
    "print(\"[1,2,3]:\", sum_list([1,2,3]))\n",
    "print(\"[1,2,3,4,5,6,7,8,9,10]:\", sum_list([1,2,3,4,5,6,7,8,9,10]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d32256b4",
   "metadata": {},
   "source": [
    "[View on PythonTutor](https://pythontutor.com/render.html#code=def%20sum_list%28list_to_sum%29%3A%0A%20%20%20%20if%20len%28list_to_sum%29%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%200%0A%20%20%20%20%0A%20%20%20%20current_value%20%3D%20list_to_sum%5B0%5D%0A%20%20%20%20recursive_value%20%3D%20sum_list%28list_to_sum%5B1%3A%5D%29%0A%20%20%20%20result%20%3D%20current_value%20%2B%20recursive_value%0A%20%20%20%20return%20result%0A%0Aprint%28%22%5B1,2,3%5D%3A%22,%20sum_list%28%5B1,2,3%5D%29%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) \n",
    "\n",
    "Note: Intermediary variables were added to the code on PythonTutor to demonstrate the program's state as shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48d4ed64",
   "metadata": {},
   "outputs": [],
   "source": [
    "def sum_list(list_to_sum):\n",
    "    if len(list_to_sum) == 0:\n",
    "        return 0\n",
    "    \n",
    "    current_value = list_to_sum[0]\n",
    "    recursive_value = sum_list(list_to_sum[1:])\n",
    "    result = current_value + recursive_value\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a6ef8a6",
   "metadata": {},
   "source": [
    "Adding intermediary variables like this may help debugging. By separating and making explicit the different steps, it becomes easier to analyze. Using a debugger, programmers can step through each code line and follow the variables' changes. The variable names provide additional context as well."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5335c165",
   "metadata": {},
   "source": [
    "## Case Study: Recursive Directory Listings"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c542557f",
   "metadata": {},
   "source": [
    "In a previous notebook, we learned the basic operations to perform directory and file operations.\n",
    "\n",
    "One common task with filesystems is to list all of the files within a particularly directory, including any subdirectories, as well as any subdirectories those may have, then subdirectories within those subdirectories, and then ...  \n",
    "\n",
    "As you can read, traversing a directory and all of the children can become complex - a priori, we do not know the depth of the directory structure. Nor do we want to code for a specific structure depth; we are after generalized solutions.\n",
    "\n",
    "Solving this for just the initial directory is simply just listing the contents of that directory.  However, as we iterate through that list, we need to ask ourselves what to do for each file based on its type:\n",
    "- file: just print the name\n",
    "- directory: print the name, and then list the contents of that directory.\n",
    "\n",
    "Ah, we have just defined the solution in terms of itself. Now, what is the base case? Is the directory empty? Sure, but no guarantee that an empty directory exists. So when do we stop making function calls? When a directory doesnʼt contain any more subdirectories. In this situation, an explicit base condition does not exist; instead, the base condition occurs implicitly based on the directory's contents."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28c30f97",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "def walk_directory(directory_name):\n",
    "    files = os.listdir(directory_name)\n",
    "    for file in files:\n",
    "        print(file)\n",
    "        test_filename = os.path.join(directory_name,file)\n",
    "        if (os.path.isdir(test_filename)):\n",
    "            walk_directory(test_filename)\n",
    "        \n",
    "walk_directory(\".\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af08a1e2",
   "metadata": {},
   "source": [
    "## Recursion Notes\n",
    "\n",
    "Two types of recursive functions exist - _head recursion_ and _tail recursion_.  \n",
    "\n",
    "For _head recursion_, the recursive call comes first, then any other processing in the function. `say_hello_increasing()` is an example of head recursion.  Note that we had the base condition check executed first. If the recursive call had occurred first, then a stack overflow error would have happened as the condition for calling the function never changed.\n",
    "\n",
    "In _tail recursion_, the function's processing comes first, followed by the recursive call.  `walk_directory()` is an example of tail recursion.\n",
    "\n",
    "Often, we may want to initiate a recursive algorithm but initially perform one or more of the following items as part of that process:\n",
    "- validate parameters\n",
    "- initialize any parameters or auxiliary variables. Often, we may want to keep track of the stack depth ourselves\n",
    "- handle any exceptions and errors. (We cover these soon.)\n",
    "\n",
    "For these situations, use a _wrapper function_ to perform these tasks. A _wrapper function_ exists where the primary purpose is to call another routine.  In this situation, we write and then utilize a wrapper function to validate the parameters and then to correctly call the recursive function.  One of the other features of Python is the ability to nest function(s) within a function. This nesting prevents code outside of our function from calling the nested functions. So external code in this situation cannot directly call the recursive function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11bd2927",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "def say_hello(num_times):\n",
    "    def say_hello_increasing(count, max_count):\n",
    "        if (count == max_count):\n",
    "            return\n",
    "        say_hello_increasing(count + 1, max_count)\n",
    "        print('hello')\n",
    "    \n",
    "    if (num_times < 0):\n",
    "        print(\"num_times cannot be negative\")\n",
    "        return\n",
    "    if (num_times >= sys.getrecursionlimit()): \n",
    "        print(\"num_times greater than or equal to the recursion limit\")\n",
    "        return\n",
    "    say_hello_increasing(0, num_times)\n",
    "    \n",
    "say_hello(-1)\n",
    "say_hello(5)\n",
    "say_hello(sys.getrecursionlimit())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48646229",
   "metadata": {},
   "source": [
    "What is the actual value for sys.getrecursionlimit() in your environment?  What is the largest value you can successfully pass to say_hello without causing an error?  Why does this discrepancy exist?\n",
    "\n",
    "Notice that the inner function `say_hello_increasing()` needed to be defined first.  Try re-writing the code such that the function definition occurs last.  What error occurred?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25ad5312",
   "metadata": {},
   "outputs": [],
   "source": [
    "# reorder say_hello such that say_hello_increasing is the first line in the function\n",
    "def say_hello(num_times):\n",
    "    def say_hello_increasing(count, max_count):\n",
    "        if (count == max_count):\n",
    "            return\n",
    "        say_hello_increasing(count + 1, max_count)\n",
    "        print('hello')\n",
    "        \n",
    "    say_hello_increasing(0, num_times)\n",
    "    \n",
    "\n",
    "\n",
    "say_hello(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9edaf18a",
   "metadata": {},
   "source": [
    "## Verifying Correctness\n",
    "To verify a recursive algorithm is correct, we need to show that both a base case and a recursive case exist within the algorithm.  \n",
    "\n",
    "We can also apply [mathematical induction](https://en.wikipedia.org/wiki/Mathematical_induction) to prove a numerical recursive algorithm works correctly.\n",
    "\n",
    "In mathematical induction, we show:\n",
    "1. The base / initial case holds. Often this is fairly trivial.  $0! = 1$\n",
    "2. Inductive step. For every $n$, we demonstrate that the algorithm also holds for $n+1$. We assume $n$ is true. For our use, we can equivalently assume $n-1$ is true, and then demonstrate that $n$ holds.\n",
    " \n",
    "Going back to the recursive algorithm for factorial:\n",
    "```python\n",
    "def factorial(n):\n",
    "    \"\"\"Return the factorial of n.\"\"\"\n",
    "    if n == 0:\n",
    "        return 1\n",
    "   else:\n",
    "        return n * factorial(n-1)\n",
    "```\n",
    "**Proof:**\n",
    "* *Basic step (n=0)*:  We can see that `factorial(0)` $= 1$\n",
    "* *Inductive step*: Assume the algorithm correctly computes (n-1)! for `factorial(n-1)`.  Then for factorial(n), the recursive case computes $n * (n-1)! = n!$, which is the correct value for `factorial(n)`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8c7e44e",
   "metadata": {},
   "source": [
    "## Common Recursive Issues\n",
    "Several issues can arise when using recursion:\n",
    "1. No update exists to the data prior to the recursive call.  In this situation, we have not reduced the problem into a smaller subprogram.  This leads to a `RecursionError` as the function will just be called from itself without any change.\n",
    "2. The base case is incorrect.  Earlier in this notebook, we showed the issue where we missed the base case due to an overly strict conditional check.  Other instances will occur when not all bases are appropriate defined.  As you will be asked in one of the exercises to compute [Fibonacci numbers](https://en.wikipedia.org/wiki/Fibonacci_number), you will see that two base cases exist.\n",
    "3. Data within the function references a mutable data structure that is changed during the function.  As many recursive calls exist,  tracking changes to the data can be problematic.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d2cd445",
   "metadata": {},
   "source": [
    "## Reentrancy\n",
    "The last issue brings up a concept within computing termed [\"reentrancy\"](https://en.wikipedia.org/wiki/Reentrancy_(computing). Functions are considered reentrant if multiple invocations can be safely executed concurrently.  Such concurrency exists on multi-processor system or when a procedure is temporarily interrupted (paused) to allow another thread/process to execute the function as well. This situation frequently occurs within web applications.  Typically, a function exists that serves as a route (an endpoint) for a particular URL. As many clients can request that URL simultaneously, the code that handles the request must allow for reentrancy.  While the Wikipedia page provides several rules functions should follow to be reentrant, the follow conditions suffice:\n",
    "- State needs to be maintained within parameters and local variables\n",
    "- Access to static variables (discussed in classes) and global variables must be read only.  This also requires that other code does not modify those variables. If those variables can be modified, the access to the variables must be synchronized. This is an advanced topic covered elsewhere.\n",
    "- The function can not share references to mutable objects (such as lists) - either within calls to itself or elsewhere within the program. Mutability means that we can not be sure that the object has been modified.\n",
    "\n",
    "You should strive to make your code reentrant whenever possible. Such code tends to avoid side-effects and is general easier to debug.  When multiple parts of a program or different processes can change problems it becomes difficult both to reproduce an issue as well as to track the issue's root cause."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38a6f92d",
   "metadata": {},
   "source": [
    "[![Relevant XKCD Cartoon](images/relevantXKCD.png)](https://thomaspark.co/2017/01/relevant-xkcd/)\n",
    "<br>\n",
    "<small>Credit: https://thomaspark.co/2017/01/relevant-xkcd/</small>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f526e19b",
   "metadata": {},
   "source": [
    "## Exercises:\n",
    "1. Write a recursive function to compute the nth [Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number).  This function should have the signature `fibonacci(n)`  Computing Fiboannic numbers is the other standard example often used for recursion.  Again, we leave it as an exercise for the reader.\n",
    "2. Sum a list. However, an element may be a number, a list, or a string. Ignore strings.To test if a variable is a string, use `type(variable_name) is str`; to test if a variable is a list, use `type(variable_name) is list`.\n",
    "3. Implement a modified version of the Linux command-line program tree as a recursive function. Below is the output. In this example, we called the function with the current directory as the argument. List the files and directories in alphabetical order. The number in parenthesis is the size of the file in bytes. At the bottom of the output, display the number of directories and files underneath the initial directory. Do not include the initial directory in the count. The final number is the total number of bytes allocated to the entire structure, including the initial directory. The starting point for the code should be the function  `tree(directory_name)`.  You can assume there will always be multiple files and directories. i.e., you do not have to special case the plurals for this problem. Show all files - including hidden files (those that start with a period).  Implementation note: to be able to reference a variable defined by an outer function within a nested function, use the `nonlocal` keyword.  This functions similarly to the `global` keyword used when discussing namespaces.\n",
    "\n",
    "<pre>\n",
    ".\n",
    "├── 00-Preliminaries.ipynb (456,333)\n",
    "├── 01-Introduction.ipynb (799,312)\n",
    "├── answers\n",
    "│   ├── 02-Answers (23,333)\n",
    "│   ├── 03-Answers (76,566)\n",
    "├── data\n",
    "│   ├── PakistanSuicideAttacks.csv (264,320)\n",
    "│   ├── djia_returns_1886_2022.csv (56,203)\n",
    "│   └── state_populations.txt (25,850)\n",
    "├── images\n",
    "│   ├── ifElifStatement.png (45,045) \n",
    "│   ├── ifElseStatement.png (47,334)\n",
    "\n",
    "3 directories, 9 files\n",
    "2,345,333 bytes\n",
    "<pre>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60cc883e",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
